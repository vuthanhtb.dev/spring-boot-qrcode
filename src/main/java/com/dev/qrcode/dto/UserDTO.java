package com.dev.qrcode.dto;

import lombok.Data;

@Data
public class UserDTO {
    private String name;
    private String email;
    private String phone;
}
