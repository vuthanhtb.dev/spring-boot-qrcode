package com.dev.qrcode.controller;

import com.dev.qrcode.dto.UserDTO;
import com.dev.qrcode.service.QrCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/qrcode")
public class QrCodeController {
    @Autowired
    QrCodeService qrCodeService;

    @PostMapping
    public ResponseEntity<String> generateQrCode(@RequestBody UserDTO data) {
        return ResponseEntity.ok(qrCodeService.generateQrCode(data));
    }
}
