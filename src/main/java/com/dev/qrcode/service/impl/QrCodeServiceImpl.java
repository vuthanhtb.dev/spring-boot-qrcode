package com.dev.qrcode.service.impl;

import com.dev.qrcode.dto.UserDTO;
import com.dev.qrcode.service.QrCodeService;
import com.dev.qrcode.utils.AppUtil;
import com.dev.qrcode.utils.Constants;
import org.springframework.stereotype.Service;

@Service
public class QrCodeServiceImpl implements QrCodeService {
    @Override
    public String generateQrCode(UserDTO data) {
        String prettyData = AppUtil.prettyObject(data);
        return AppUtil.generateQrCode(prettyData, Constants.ORDER_QR_CODE_SIZE_WIDTH, Constants.ORDER_QR_CODE_SIZE_HEIGHT);
    }
}
