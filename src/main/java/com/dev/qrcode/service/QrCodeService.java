package com.dev.qrcode.service;

import com.dev.qrcode.dto.UserDTO;

public interface QrCodeService {
    String generateQrCode(UserDTO data);
}
